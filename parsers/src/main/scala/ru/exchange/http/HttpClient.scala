package ru.exchange.http

import org.jsoup.nodes.Document
import org.jsoup.Jsoup

class HttpClient extends BaseHttpClient {

  //val httpClient = new uk.co.bigbeeconsultants.http.HttpClient

//  def pairToCookie(domain: String, pair: (String, String)) = Cookie(pair._1, pair._2, new Domain(domain))
//
//  def cookiesToCookieJar(cookieDomain: String, cookies: Seq[(String, String)]): CookieJar = {
//    CookieJar(cookies.map(cookie => pairToCookie(cookieDomain, cookie)).toList)
//  }

  /**
   * @todo add cookie support
   */
  override def get(url: String, cookies: (String, String) *): Document = {
    val connection = Jsoup.connect(url)
    cookies.foreach(c => connection.cookie(c._1, c._2))
    connection.followRedirects(true)
    connection.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36")
    connection.get
  }

  //override def post(url: String, cookies: (String, String) *): String = ???

}