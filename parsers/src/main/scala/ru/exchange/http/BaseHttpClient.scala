package ru.exchange.http

import org.jsoup.nodes.Document

trait BaseHttpClient {

  def get(url: String, cookies: (String, String) *): Document

}
