package ru.exchange.parser

import scala.util.Try
import ru.exchange.http.{HttpClient, BaseHttpClient}
import ru.exchange.protocol.{Currency, CityCode, BankCode}

class SberbankParser(httpClient: BaseHttpClient = new HttpClient()) extends BaseParser with DecimalFormatHelper {

  val code = BankCode.Sberbank

  val cities = Map(
    CityCode.Moscow -> "moscow",
    CityCode.SaintPetersburg -> "saintpetersburg",
    CityCode.Novosibirsk -> "novosibirsk",
    CityCode.Ekaterinburg -> "sverdlovsk",
    CityCode.NigniyNovgorog -> "nizhnynovgorod",
    CityCode.Kazan -> "tatarstan",
    CityCode.Samara -> "samara",
    CityCode.Omsk -> "omsk",
    CityCode.Chelabinsk -> "chelyabinsk",
    CityCode.RostovOnDone -> "rostov",
    CityCode.Ufa -> "bashkortostan",
    CityCode.Volgograd -> "volgograd",
    CityCode.Krasnoyarsk -> "krasnoyarsk",
    CityCode.Perm -> "perm",
    CityCode.Voronej -> "voronezh",
    CityCode.Majkop -> "adygea",
    CityCode.Belgorod -> "belgorod",
    CityCode.Yaroslavl -> "yaroslavl",
    CityCode.Orel -> "oryol",
    CityCode.Smolensk -> "smolensk",
    CityCode.Tambov -> "tambov",
    CityCode.Tver -> "tver",
    CityCode.Bryansk -> "bryansk",
    CityCode.Vladimir -> "vladimir",
    CityCode.Ijevsk -> "udmurtia",
    CityCode.Tumen -> "tyumen",
    CityCode.Kursk -> "kursk",
    CityCode.UjnoSahalinsk -> "sakhalin",
    CityCode.Irkutsk -> "irkutsk",
    CityCode.Salehard -> "yamalonenets",
    CityCode.Mahachkala -> "dagestan",
    CityCode.Ivanovo -> "ivanovo",
    CityCode.Arhangelsk -> "arkhangelsk",
    CityCode.Orenburg -> "orenburg"
  )

  def getUrl(city: String) = s"http://www.sberbank.ru/$city/ru/quotes/currencies/"


  def parse(): Seq[ParseResult] = {

    cities.map {
      case (cityCode, cityParserCode) =>
        val result = httpClient.get(getUrl(cityParserCode))
        val rowUsd = result.select(".table3_eggs4  tr:eq(1)")

        val buyUsd = rowUsd.select("td:eq(2)")
        val sellUsd = rowUsd.select("td:eq(4)")

        val buyResultUsd = Try(decimalCommaFormat.parse(buyUsd.text.trim).doubleValue()).toOption
        val sellResultUsd = Try(decimalCommaFormat.parse(sellUsd.text.trim).doubleValue()).toOption

        val rowEur = result.select(".table3_eggs4  tr:eq(2)")

        val buyEur = rowEur.select("td:eq(2)")
        val sellEur = rowEur.select("td:eq(4)")

        val buyResultEur = Try(decimalCommaFormat.parse(buyEur.text.trim).doubleValue()).toOption
        val sellResultEur = Try(decimalCommaFormat.parse(sellEur.text.trim).doubleValue()).toOption

        Seq(ParseResult(ParseParameter(cityCode, code), sellResultUsd, buyResultUsd, Currency.USD),
            ParseResult(ParseParameter(cityCode, code), sellResultEur, buyResultEur, Currency.EUR))
    }.flatten.toSeq

  }

}
