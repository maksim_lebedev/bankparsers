package ru.exchange.parser

import ru.exchange.http.{HttpClient, BaseHttpClient}
import ru.exchange.protocol.{Currency, CityCode, BankCode}

import scala.util.Try

class RosCapitalBankParser(httpClient: BaseHttpClient = new HttpClient()) extends BaseParser with DecimalFormatHelper {

  val code = BankCode.RoscapitalBank

  val cities = Map(
    CityCode.Moscow -> "moscow",
    CityCode.SaintPetersburg -> "saintpetersburg",
    CityCode.Novosibirsk -> "novosibirsk",
    CityCode.Ekaterinburg -> "sverdlovsk",
    CityCode.NigniyNovgorog -> "nizhnynovgorod",
    CityCode.Kazan -> "tatarstan",
    CityCode.Samara -> "samara",
    CityCode.Omsk -> "omsk",
    CityCode.Chelabinsk -> "chelyabinsk",
    CityCode.RostovOnDone -> "rostov",
    CityCode.Ufa -> "bashkortostan",
    CityCode.Volgograd -> "volgograd",
    CityCode.Krasnoyarsk -> "krasnoyarsk",
    CityCode.Perm -> "perm",
    CityCode.Voronej -> "voronezh",
    CityCode.Majkop -> "adygea",
    CityCode.Belgorod -> "belgorod",
    CityCode.Yaroslavl -> "yaroslavl",
    CityCode.Orel -> "oryol",
    CityCode.Smolensk -> "smolensk",
    CityCode.Tambov -> "tambov",
    CityCode.Tver -> "tver",
    CityCode.Bryansk -> "bryansk",
    CityCode.Vladimir -> "vladimir",
    CityCode.Ijevsk -> "udmurtia",
    CityCode.Tumen -> "tyumen",
    CityCode.Kursk -> "kursk",
    CityCode.UjnoSahalinsk -> "sakhalin",
    CityCode.Irkutsk -> "irkutsk",
    CityCode.Salehard -> "yamalonenets",
    CityCode.Mahachkala -> "dagestan",
    CityCode.Ivanovo -> "ivanovo",
    CityCode.Arhangelsk -> "arkhangelsk",
    CityCode.Orenburg -> "orenburg"
  )

  def getUrl = s"http://www.roscap.ru/"

  def parse(): Seq[ParseResult] = {

    cities.map {
      city =>
        val result = httpClient.get(getUrl)
        val rowUsd = result.select("div#currency  tr:eq(1)")
        val buyUsd = rowUsd.select("td:eq(1)")
        val sellUsd = rowUsd.select("td:eq(1)")

        val buyResultUsd = Try(decimalCommaFormat.parse(buyUsd.text.trim).doubleValue()).toOption
        val sellResultUsd = Try(decimalCommaFormat.parse(sellUsd.text.trim).doubleValue()).toOption

        val rowEur = result.select("div#currency  tr:eq(2)")

        val buyEur = rowEur.select("td:eq(1)")
        val sellEur = rowEur.select("td:eq(1)")

        val buyResultEur = Try(decimalCommaFormat.parse(buyEur.text.trim).doubleValue()).toOption
        val sellResultEur = Try(decimalCommaFormat.parse(sellEur.text.trim).doubleValue()).toOption

        Seq(ParseResult(ParseParameter(city._1, code), sellResultUsd, buyResultUsd, Currency.USD),
          ParseResult(ParseParameter(city._1, code), sellResultEur, buyResultEur, Currency.EUR))
    }.flatten.toSeq
  }
}
