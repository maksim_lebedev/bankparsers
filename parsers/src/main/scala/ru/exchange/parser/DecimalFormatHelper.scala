package ru.exchange.parser

/**
 * @todo replace to object
 */
trait DecimalFormatHelper {
  val decimalCommaFormat = new java.text.DecimalFormat("#0,0000")
  val decimalDotFormat = new java.text.DecimalFormat("#0.0000")
}
