package ru.exchange.parser

import ru.exchange.protocol.Currency

case class ParseResult(param: ParseParameter, sell: Option[Double], buy: Option[Double], currency: Currency.Value)
