package ru.exchange.parser

import ru.exchange.http.{HttpClient, BaseHttpClient}
import ru.exchange.protocol.{Currency, CityCode, BankCode}

import scala.util.Try

class OtpBankParser(httpClient: BaseHttpClient = new HttpClient()) extends BaseParser with DecimalFormatHelper {

  val code = BankCode.OtpBank

  val cities = Map(
    CityCode.Volgograd -> "00080",
    CityCode.Ekaterinburg -> "00049",
    CityCode.Irkutsk -> "00011",
    CityCode.Kazan -> "00035",
    CityCode.Krasnoyarsk -> "00012",
    CityCode.Moscow -> "00001",
    CityCode.NigniyNovgorog -> "00094",
    CityCode.Novosibirsk -> "00005",
    CityCode.Omsk -> "00007",
    CityCode.Perm -> "00053",
    CityCode.RostovOnDone -> "00082",
    CityCode.Samara -> "00003",
    CityCode.SaintPetersburg -> "00044",
    CityCode.Tver -> "00057",
    CityCode.Tumen -> "00055",
    CityCode.Ufa -> "00042",
    CityCode.Chelabinsk -> "00056",
    CityCode.Yaroslavl -> "00067"
  )

  def getUrl = s"http://www.otpbank.ru/about/currency/"

  def parse(): Seq[ParseResult] = {

    cities.map {
      city =>
        val result = httpClient.get(getUrl)
        val rowUsd = result.select(".currency_ratesin_content  tr:eq(1)")

        val buyUsd = rowUsd.select("td:eq(1)")
        val sellUsd = rowUsd.select("td:eq(2)")

        val buyResultUsd = Try(decimalCommaFormat.parse(buyUsd.text.trim).doubleValue()).toOption
        val sellResultUsd = Try(decimalCommaFormat.parse(sellUsd.text.trim).doubleValue()).toOption

        val rowEur = result.select(".currency_ratesin_content  tr:eq(2)")

        val buyEur = rowEur.select("td:eq(1)")
        val sellEur = rowEur.select("td:eq(2)")

        val buyResultEur = Try(decimalCommaFormat.parse(buyEur.text.trim).doubleValue()).toOption
        val sellResultEur = Try(decimalCommaFormat.parse(sellEur.text.trim).doubleValue()).toOption

        Seq(ParseResult(ParseParameter(city._1, code), sellResultUsd, buyResultUsd, Currency.USD),
          ParseResult(ParseParameter(city._1, code), sellResultEur, buyResultEur, Currency.EUR))
    }.flatten.toSeq
  }

}
