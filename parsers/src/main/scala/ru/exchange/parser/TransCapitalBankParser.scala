package ru.exchange.parser

import ru.exchange.http.{HttpClient, BaseHttpClient}
import ru.exchange.protocol.{Currency, CityCode, BankCode}

import scala.util.Try

class TransCapitalBankParser(httpClient: BaseHttpClient = new HttpClient()) extends BaseParser with DecimalFormatHelper {

  val code = BankCode.TransCapitalBank

  val cities = Map(
    CityCode.Belgorod -> "78199",
    CityCode.Bryansk -> "241",
    CityCode.Voronej -> "242",
    CityCode.Ekaterinburg -> "243",
    CityCode.Moscow -> "239",
    CityCode.NigniyNovgorog -> "246",
    CityCode.Novosibirsk -> "247",
    CityCode.Perm -> "248",
    CityCode.RostovOnDone -> "250",
    CityCode.Samara -> "251",
    CityCode.SaintPetersburg -> "240",
    CityCode.Tumen -> "253",
    CityCode.Yaroslavl -> "254"
  )

  def getUrl = s"http://www.transcapital.ru/"

  def parse(): Seq[ParseResult] = {

    cities.map {
      city =>
        val result = httpClient.get(getUrl)
        val rowUsd = result.select(".info  tr:eq(1)").first()

        val buyUsd = rowUsd.select("td:eq(1)")
        val sellUsd = rowUsd.select("td:eq(2)")

        val buyResultUsd = Try(decimalCommaFormat.parse(buyUsd.text.trim).doubleValue()).toOption
        val sellResultUsd = Try(decimalCommaFormat.parse(sellUsd.text.trim).doubleValue()).toOption

        val rowEur = result.select(".info  tr:eq(2)").first()

        val buyEur = rowEur.select("td:eq(1)")
        val sellEur = rowEur.select("td:eq(2)")

        val buyResultEur = Try(decimalCommaFormat.parse(buyEur.text.trim).doubleValue()).toOption
        val sellResultEur = Try(decimalCommaFormat.parse(sellEur.text.trim).doubleValue()).toOption

        Seq(ParseResult(ParseParameter(city._1, code), sellResultUsd, buyResultUsd, Currency.USD),
          ParseResult(ParseParameter(city._1, code), sellResultEur, buyResultEur, Currency.EUR))
    }.flatten.toSeq
  }
}
