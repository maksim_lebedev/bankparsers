package ru.exchange.parser

trait BaseParser {

  def parse(): Seq[ParseResult]

}
