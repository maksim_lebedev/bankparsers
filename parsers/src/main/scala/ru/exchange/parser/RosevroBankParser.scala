package ru.exchange.parser

import ru.exchange.http.{HttpClient, BaseHttpClient}
import ru.exchange.protocol.{Currency, CityCode, BankCode}

import scala.util.Try

class RosevroBankParser(httpClient: BaseHttpClient = new HttpClient()) extends BaseParser with DecimalFormatHelper {

  val code = BankCode.RosevroBank

  val cities = Map(
    CityCode.Moscow -> "365",
    CityCode.SaintPetersburg -> "405",
    CityCode.Ekaterinburg -> "6011",
    CityCode.Novosibirsk -> "395",
    CityCode.RostovOnDone -> "5814",
    CityCode.Samara -> "397",
    CityCode.Chelabinsk -> "402",
    CityCode.Belgorod -> "359",
    CityCode.Vladimir -> "363",
    CityCode.Voronej -> "47020",
    CityCode.Perm -> "30004",
    CityCode.Tver -> "5833"
  )

  def getUrl(city: String) = s"http://www.rosevrobank.ru/?REGIONS=$city"

  def parse(): Seq[ParseResult] = {

    cities.map {
      case (cityCode, cityParserCode) =>
        val result = httpClient.get(getUrl(cityParserCode))
        val rowUsd = result.select(".rate  tbody:eq(1)").select("tr:eq(0)")

        val buyUsd = rowUsd.select("td:eq(1)")
        val sellUsd = rowUsd.select("td:eq(2)")

        val buyResultUsd = Try(decimalCommaFormat.parse(buyUsd.text.trim).doubleValue()).toOption
        val sellResultUsd = Try(decimalCommaFormat.parse(sellUsd.text.trim).doubleValue()).toOption

        val rowEur = result.select(".rate  tbody:eq(1)").select("tr:eq(1)")

        val buyEur = rowEur.select("td:eq(1)")
        val sellEur = rowEur.select("td:eq(2)")

        val buyResultEur = Try(decimalCommaFormat.parse(buyEur.text.trim).doubleValue()).toOption
        val sellResultEur = Try(decimalCommaFormat.parse(sellEur.text.trim).doubleValue()).toOption

        Seq(ParseResult(ParseParameter(cityCode, code), sellResultUsd, buyResultUsd, Currency.USD),
          ParseResult(ParseParameter(cityCode, code), sellResultEur, buyResultEur, Currency.EUR))
    }.flatten.toSeq
  }

}
