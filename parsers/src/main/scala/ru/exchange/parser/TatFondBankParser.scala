package ru.exchange.parser

import ru.exchange.http.{HttpClient, BaseHttpClient}
import ru.exchange.protocol.{Currency, CityCode, BankCode}

import scala.util.Try

class TatFondBankParser(httpClient: BaseHttpClient = new HttpClient()) extends BaseParser with DecimalFormatHelper {

  val code = BankCode.TatFondBank

  val cities = Map(
    CityCode.Voronej -> "270",
    CityCode.Ekaterinburg -> "628",
    CityCode.Ijevsk -> "194",
    CityCode.Moscow -> "16",
    CityCode.Novosibirsk -> "novosibirsk",
    CityCode.Perm -> "80",
    CityCode.SaintPetersburg -> "81",
    CityCode.Yaroslavl -> "361",
    CityCode.Ufa -> "254",
    CityCode.Kazan -> "14",
    CityCode.NigniyNovgorog -> "100",
    CityCode.Samara -> "104"
  )

  def getUrl(city: String) = s"https://tfb.ru/?cityselect=$city"

  def parse(): Seq[ParseResult] = {

    cities.map {
      case(cityCode, cityParserCode) =>
        val result = httpClient.get(getUrl(cityParserCode))
        val rowUsd = result.select(".usd").first()

        val buyUsd = rowUsd.select("td:eq(1)")
        val sellUsd = rowUsd.select("td:eq(2)")

        val buyResultUsd = Try(decimalCommaFormat.parse(buyUsd.text.trim).doubleValue()).toOption
        val sellResultUsd = Try(decimalCommaFormat.parse(sellUsd.text.trim).doubleValue()).toOption

        val rowEur = result.select(".euro").first()

        val buyEur = rowEur.select("td:eq(1)")
        val sellEur = rowEur.select("td:eq(2)")

        val buyResultEur = Try(decimalCommaFormat.parse(buyEur.text.trim).doubleValue()).toOption
        val sellResultEur = Try(decimalCommaFormat.parse(sellEur.text.trim).doubleValue()).toOption

        Seq(ParseResult(ParseParameter(cityCode, code), sellResultUsd, buyResultUsd, Currency.USD),
          ParseResult(ParseParameter(cityCode, code), sellResultEur, buyResultEur, Currency.EUR))
    }.flatten.toSeq
  }

}
