package ru.exchange.parser

import ru.exchange.protocol.{CityCode, BankCode}

case class ParseParameter(cityCode: CityCode.Value, bankCode: BankCode.Value)
