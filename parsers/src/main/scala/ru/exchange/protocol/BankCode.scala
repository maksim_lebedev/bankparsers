package ru.exchange.protocol

object BankCode extends Enumeration {

  type BankCode = Value

  val Rosbank = Value(1, "rosbank")
  val MoscowBank = Value(2, "moscowbank")
  val Sberbank = Value(3, "sberbank")
  val Vtb24 = Value(4, "vtb24")
  val UnicreditBank = Value(5, "unicreditbank")
  val PromsvyazBank = Value(6, "promsvyaznank")
  val MoscowCreditBank = Value(7, "mcb")
  val RussiaBank = Value(8, "abr")
  val RussianStandardBank = Value(9, "rsb")
  val AkBarsBank = Value(10, "akbars")
  val UralSibBank = Value(11, "uralsib")
  val BinBank = Value(12, "binbank")
  val HomeCreditBank = Value(13, "homecredit")
  val RosselhozBank = Value(14, "rosselhoz")
  val PetrokomerzBank = Value(15, "petrokomerz")
  val VostochniyExpressBank = Value(16, "vostochniyexpress")
  val te = Value(17, "raiffeisen")
  val OpenBank = Value(18, "openbank")
  val NordeaBank = Value(19, "nordea")
  val SvyazBank = Value(20, "svyaz")
  val GlobeksBank = Value(21, "globeks")
  val MosoblBank = Value(22, "mosobl")
  val SmpBank = Value(23, "smp")
  val SovcomBank = Value(43, "sovcombank")
  val OtpBank = Value(44, "otpbank")
  val TatFondBank = Value(45, "tatfondbank")
  val TransCapitalBank = Value(46, "transcapitalbank")
  val RosevroBank = Value(48, "rosevrobank")
  val RoscapitalBank = Value(49, "roscapitalbank")
}
