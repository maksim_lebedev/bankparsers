package ru.exchange.protocol

object Currency extends Enumeration {

  type CurrencyCode = Value

  val USD = Value(1, "usd")
  val EUR = Value(2, "eur")

}
