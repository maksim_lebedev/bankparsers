package ru.exchange.protocol

/**
 * @todo review names
 */
object CityCode extends Enumeration {

  type CityCode = Value

  val Moscow = Value(1)
  val SaintPetersburg = Value(2)
  val Novosibirsk = Value(3)
  val Ekaterinburg = Value(4)
  val NigniyNovgorog = Value(5)
  val Kazan = Value(6)
  val Samara = Value(7)
  val Omsk = Value(8)
  val Chelabinsk = Value(9)
  val RostovOnDone = Value(10)
  val Ufa = Value(11)
  val Volgograd = Value(12)
  val Krasnoyarsk = Value(13)
  val Perm = Value(14)
  val Voronej = Value(15)
  val Majkop = Value(16)
  val Belgorod = Value(17)
  val Yaroslavl = Value(18)
  val Orel = Value(19)
  val Smolensk = Value(20)
  val Tambov = Value(21)
  val Tver = Value(22)
  val Bryansk = Value(23)
  val Vladimir = Value(24)
  val Ijevsk = Value(25)
  val Tumen = Value(26)
  val Kursk = Value(27)
  val UjnoSahalinsk = Value(28)
  val Salehard = Value(29)
  val Irkutsk = Value(30)
  val Mahachkala = Value(31)
  val Ivanovo = Value(32)
  val Arhangelsk = Value(33)
  val Orenburg = Value(34)
}
