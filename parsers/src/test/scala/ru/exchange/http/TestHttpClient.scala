package ru.exchange.http

import org.jsoup.nodes.Document
import java.io.File
import org.jsoup.Jsoup

/**
 * @todo change constructor to BankCode
 */
class TestHttpClient(code: String) extends BaseHttpClient {

  val prefix = "parsers"

  val file = "data.txt"

  def get(url: String, cookies: (String, String) *): Document = {
    Jsoup.parse(new File(getClass.getResource(s"/$prefix/$code/$file").getPath), "UTF-8")
  }

}
