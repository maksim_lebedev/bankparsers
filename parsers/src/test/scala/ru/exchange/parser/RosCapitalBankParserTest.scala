package ru.exchange.parser

import ru.exchange.http.TestHttpClient
import ru.exchange.protocol.{Currency, BankCode}

class RosCapitalBankParserTest extends UnitSpec {

  "RosCapitalBank parser" - {

    "should correct parse value" in {
      val parser = new RosCapitalBankParser(new TestHttpClient("roscapitalbank"))

      val result = parser.parse()

      assume(result.size > 0, "RosCapitalBank parse result must return non empty result")

      println(result.toString())

      result.foreach { value =>
        value.param.bankCode shouldBe BankCode.RoscapitalBank
        value.param.cityCode.toString.length should be > 0
        value.currency match {
          case Currency.USD =>
            value.buy shouldBe Some(64.99)
            value.sell shouldBe Some(65.56)
          case Currency.EUR =>
            value.buy shouldBe Some(75.27)
            value.sell shouldBe Some(75.82)
        }
      }
    }

  }

}
