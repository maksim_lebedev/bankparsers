package ru.exchange.parser

import ru.exchange.http.TestHttpClient
import ru.exchange.protocol.{Currency, BankCode}

class OtpBankParserTest extends UnitSpec {

  "OtpBank parser" - {

    "should correct parse value" in {
      val parser = new OtpBankParser(new TestHttpClient("otpbank"))

      val result = parser.parse()

      assume(result.size > 0, "OtpBank parse result must return non empty result")

      println(result.toString())

      result.foreach { value =>
        value.param.bankCode shouldBe BankCode.OtpBank
        value.param.cityCode.toString.length should be > 0
        value.currency match {
          case Currency.USD =>
            value.buy shouldBe Some(62.00)
            value.sell shouldBe Some(68.00)
          case Currency.EUR =>
            value.buy shouldBe Some(73.00)
            value.sell shouldBe Some(79.00)
        }
      }
    }

  }

}
