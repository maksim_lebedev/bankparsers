package ru.exchange.parser

import ru.exchange.http.TestHttpClient
import ru.exchange.protocol.{Currency, BankCode}

class SberbankParserTest extends UnitSpec {

  "Sberbank parser" - {

    "should correct parse value" in {
      val parser = new SberbankParser(new TestHttpClient("sberbank"))

      val result = parser.parse()

      assume(result.size > 0, "Sberbank parse result must return non empty result")

      println(result.toString())

      result.foreach { value =>
        value.param.bankCode shouldBe BankCode.Sberbank
        value.param.cityCode.toString.length should be > 0
        value.currency match {
          case Currency.USD =>
            value.buy shouldBe Some(35.27)
            value.sell shouldBe Some(36.77)
          case Currency.EUR =>
            value.buy shouldBe Some(49.1)
            value.sell shouldBe Some(50.6)
        }
      }
    }

  }

}
