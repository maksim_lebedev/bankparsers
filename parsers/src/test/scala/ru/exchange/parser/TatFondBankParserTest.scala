package ru.exchange.parser

import ru.exchange.http.TestHttpClient
import ru.exchange.protocol.{Currency, BankCode}

class TatFondBankParserTest extends UnitSpec {

  "TatFondBank parser" - {

    "should correct parse value" in {
      val parser = new TatFondBankParser(new TestHttpClient("tatfondbank"))

      val result = parser.parse()

      assume(result.size > 0, "TatFondBank parse result must return non empty result")

      println(result.toString())

      result.foreach { value =>
        value.param.bankCode shouldBe BankCode.TatFondBank
        value.param.cityCode.toString.length should be > 0
        value.currency match {
          case Currency.USD =>
            value.buy shouldBe Some(63.50)
            value.sell shouldBe Some(67.00)
          case Currency.EUR =>
            value.buy shouldBe Some(73.00)
            value.sell shouldBe Some(77.50)
        }
      }
    }

  }
}
