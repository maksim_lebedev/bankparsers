import sbt._
import Keys._
//import play.{Projeqct => PlayProject, Keys => PlayKeys}
//import play.Project._

object Dependencies {
  val scalaTest = "org.scalatest" %% "scalatest" % "2.2.0" % "test"
  val jsoup = "org.jsoup" % "jsoup" % "1.6.1"
  //val mysqlJdbc = "mysql" % "mysql-connector-java" % "5.1.27"
}

object Settings {

  lazy val projectVersion = "0.1.5"

  lazy val mainSettings: Seq[Def.Setting[_]] = Defaults.coreDefaultSettings ++ Seq(
    scalaVersion := "2.11.4",
    scalacOptions ++= Seq("-deprecation", "-unchecked", "-feature", "-target:jvm-1.7"),
    version := projectVersion,
    javacOptions ++= Seq("-source", "1.7", "-target", "1.7")
  )

}

object Resolvers {
  val typesafeReleases = "Typesafe Releases Repository" at "http://repo.typesafe.com/typesafe/releases/"
}

object ExchangeApp extends Build {

  import Dependencies._
  import Settings._
  import Resolvers._

  lazy val parsers = Project("parsers", file("parsers"), settings = parserSettings)

  lazy val parserSettings: Seq[Def.Setting[_]] = mainSettings ++ Seq(
    name := "parsers",
    libraryDependencies ++= Seq(scalaTest, jsoup),
    testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-oDS")
  )

}